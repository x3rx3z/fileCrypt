Encypt files using AES-256.  Requires Python3 and PyCrypto.

Main features;
- Uses getPass to enter key away from prying eyes and bash history
- Uses SHA256 of the key to increase entropy and ensure AES-256 is implemented regardless of the key length