#!/usr/bin/python3

import hashlib, struct, os, sys, getpass, random
from Crypto.Cipher import AES

def encrypt( key, inFile, outFile=None, chunkSz=64*1024 ):
    # Create random IV
    iv = os.urandom(16)
    cipher = AES.new( key, AES.MODE_CBC, iv )
    # Create a default file extension
    if outFile is None: outFile = inFile + ".crypt"
    # Pack in original file size to truncate padding on decrypt
    inFile_sz = os.path.getsize( inFile )

    # Open files
    with open( inFile, 'rb' ) as infile:
        with open( outFile, 'wb' ) as outfile:
            # Write the original filesize as long long.
            # This will make truncating padding easier on decryption
            # Doesn't really reduce preimage resistance since plaintext
            # size is known (to nearest block size) from the ciphertext
            # length anyway - security through obscurity doesn't work.
            outfile.write( struct.pack('<Q', inFile_sz) )
            # Write the IV
            outfile.write( iv )

            # Read chunks from input to speed things up (multiple of 32)
            while True:
                chunk = infile.read( chunkSz )
                chunkLen = len(chunk)
                # Break at EOF
                if chunkLen == 0: break
                # Pad out short chunk
                elif chunkLen % 32 != 0:
                    chunk += '\x00'.encode('utf-8') * (32 - chunkLen % 32)
                # Write encrypted chunk to output
                outfile.write( cipher.encrypt( chunk ) )


def decrypt(key, inFile, outFile=None, chunkSz=64*1024):
    # Remove trailing .crypt extension - allowing for '.'
    # in the origninal filename.
    if outFile is None:
        outFile = ".".join( inFile.split('.')[:-1] )
    # Open the encrypted file
    with open( inFile, 'rb' ) as infile:
        # Read the original file size (unsigned long long)
        outFile_sz = struct.unpack('<Q', infile.read(struct.calcsize('Q')))[0]
        # Read the IV used for encryption
        iv = infile.read(16)

        cipher = AES.new( key, AES.MODE_CBC, iv )
        # Open the output file
        with open( outFile, 'wb' ) as outfile:
            # Decrypt chunks and write them
            while True:
                chunk = infile.read( chunkSz )
                if len( chunk ) == 0: break
                outfile.write( cipher.decrypt( chunk ) )
            # Truncate file from any padding
            outfile.truncate( outFile_sz )



if __name__ == "__main__":
    # Check argument length
    if len( sys.argv ) != 3:
        print("Usage ./fileCrypt.py -[e/d] {file}")

    # Get filename and check it's a file, not directory
    tgtFile = sys.argv[2]
    if not os.path.isfile( tgtFile ):
        print("Can only encrypt files at this stage... Exiting")
        exit()

    # Enter key to keep it out of bash history
    pwd = getpass.getpass('Key: ').encode('utf-8')
    key = hashlib.sha256( pwd ).digest()

    # Encrypt, decrypt, or die
    if sys.argv[1] == "-d":
        decrypt(key, tgtFile)
    elif sys.argv[1] == "-e":
        encrypt(key, tgtFile)
    else:
        print("Unknown flag \'"+ sys.argv[1] + "\'")
        exit()
